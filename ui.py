import Queue
import threading

from Tkinter import *

from cfour import ConnectFour


class TextUI(object):
    def update_board(self, board):
        print board

    def get_move(self, name):
        return raw_input(name + "'s turn. Enter a column number to move: ")

    def gameover(self, winner):
        print winner, "won the game!"

    def draw(self):
        print "The game is a draw!"

    def mainloop(self):
        game = ConnectFour(ui=self)
        game.play()


class GraphicUI(object):
    def __init__(self, color_map={'X': '#c02090', 'O': '#d0c000'}):
        super(GraphicUI, self).__init__()
        self.color_map = color_map
        self.user_input_queue = Queue.Queue()
        self.setup_widgets()

    def setup_widgets(self):
        root = self.root = Tk()
        board_frame = Frame(root)
        board_frame.pack(expand=1, fill=BOTH)
        self.buttons = []
        for i in range(6):
            self.buttons.append([])
            for j in range(7):
                button = Button(board_frame, text='_', width=10, height=5)
                button.config(command=lambda col=j: self.clicked(col))
                button.grid(row=i, column=j)
                self.buttons[-1].append(button)
        self.status = Label(text='')
        self.status.pack(expand=1, fill=BOTH)

    def clicked(self, column):
        self.user_input_queue.put(column)

    def mainloop(self):
        game = ConnectFour(ui=self)
        self.root.after(1000, lambda: threading.Thread(target=game.play).start())
        self.root.mainloop()

    def get_move(self, board):
        return str(self.user_input_queue.get() + 1)

    def gameover(self, winner):
        self.status.config(text='{} won the game!'.format(winner))

    def draw(self):
        self.status.config(text='The game is a draw')

    def update_board(self, board):
        for i, row in enumerate(reversed(board.board)):
            for j, cell in enumerate(row):
                if str(cell) == '_': continue
                self.buttons[i][j].config(text=str(cell))
                self.buttons[i][j].config(bg=self.color_map[str(cell)])
