import unittest

import cfour


class FakeUI:
    def set_move(self, m):
        self.move = m

    def get_move(self, board):
        return self.move


class TestGame(unittest.TestCase):
    def test_read_player_move__empty_board(self):
        ui = FakeUI()
        g = cfour.ConnectFour(ui=ui)
        g.current_player = g.adversaries[0]
        ui.set_move('1')
        self.assertEqual(g.read_player_move(), (0, 0))

    def test_read_player_move__ignore_invalid_move__full_column(self):
        ui = FakeUI()
        g = cfour.ConnectFour(ui=ui)
        g.board.set_cell((0, 1), 'X')
        g.board.set_cell((1, 1), 'O')
        g.board.set_cell((2, 1), 'X')
        g.board.set_cell((3, 1), 'O')
        g.board.set_cell((4, 1), 'X')
        g.board.set_cell((5, 1), 'O')
        g.current_player = g.adversaries[0]
        ui.set_move('2')
        self.assertEqual(g.read_player_move(), None)


if __name__ == '__main__':
    unittest.main()
