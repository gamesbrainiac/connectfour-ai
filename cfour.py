import sys

from boards import Board
from players import HumanPlayer, ComputerPlayer


class ConnectFour(object):
    def __init__(self, ui, cell_list=None, columns=7, column_size=6):
        self.ui = ui
        self.adversaries = [HumanPlayer(ui), ComputerPlayer()]
        self.board = Board()

    def read_player_move(self):
        move = self.current_player.move(self.board)
        if not isinstance(move, tuple):
            move = int(move) - 1
            possible_moves = self.board.list_possible_moves()
            position = next((elem for elem in possible_moves if elem[1] == move), None)
            if position is None:
                return None
            move = position
        return move

    def play(self):
        self.current_player = [player for player in self.adversaries
                               if player.turn is True][0]
        while not self.board.check_for_win():
            self.ui.update_board(self.board)
            move = self.read_player_move()
            if move in ('q', 'Q'):
                sys.exit(0)
            if move is None:
                continue
            self.board.set_cell(move, self.current_player.name)
            if self.board.draw():
                self.ui.update_board(self.board)
                self.ui.draw()
                return
            self.current_player = self.get_next_player()[0]

        self.ui.update_board(self.board)
        self.current_player = self.get_next_player()[0]
        self.ui.gameover(self.current_player.name)

    def get_next_player(self):
        for player in self.adversaries:
            player.turn = not player.turn
        return [player for player in self.adversaries if player.turn is True]
