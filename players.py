"""
Connect 4 for Python
"""
import copy


class HumanPlayer(object):
    def __init__(self, ui, turn=True, name='X'):
        self.ui = ui
        self.name = name
        self.turn = turn

    def check_valid_input(self, user_input, board):
        user_input = user_input.lower()
        return (user_input in
                [str(x) for x in range(1, board.columns + 1)] + ['q'])

    def move(self, board):
        # add a move function for the human
        while True:
            move = self.ui.get_move(self.name)
            if self.check_valid_input(move, board):
                return move


class ComputerPlayer(object):
    def __init__(self, turn=False, name='O', opponent_name='X', current_board=None):
        self.name = name
        self.turn = turn
        self.opponent = opponent_name

    def move(self, board):
        """ Computer move
        """
        possible_moves = board.list_possible_moves()
        pos_win = self.check_for_immediate_win_or_loss(board, possible_moves)

        if pos_win:
            return pos_win
        return self.make_move(board, possible_moves)

    def verify_possible_moves(self, board, possible_moves, player_name, check_value):
        """ Test the possible moves for win states
        """
        combination_directions = board.combination_directions + [('s', 'n')]
        for move in possible_moves:
            cell = board.get_cell(move)
            for direction in combination_directions:
                board.set_cell(move, player_name)
                if (board.count_chains_by_cell_val(cell, direction[0]) +
                        board.count_chains_by_cell_val(cell, direction[1])) == check_value:
                    board.set_cell_to_empty(move)
                    return move
                board.set_cell_to_empty(move)
        return False

    def check_for_immediate_win_or_loss(self, board, possible_moves):
        """ checks for immediate win
        """
        return_value = self.verify_possible_moves(board, possible_moves, self.name, 3)
        if not return_value:
            return_value = self.verify_possible_moves(board, possible_moves, self.opponent, 3)
        return return_value

    def make_move(self, board, possible_moves):
        """ finds a move 
        """
        evaluating_moves = []
        new_board = copy.deepcopy(board)
        comp_move_utilities = self.return_naive_utility(
            new_board, possible_moves, self.name)
        opp_move_utilities = self.return_naive_utility(
            new_board, possible_moves, self.opponent)
        evaluating_moves = comp_move_utilities + opp_move_utilities
        max_value = evaluating_moves.pop(
            evaluating_moves.index(max(evaluating_moves, key=lambda tup: tup[1])))
        new_board.set_cell(max_value[0], self.name)
        while self.verify_possible_moves(
                new_board, new_board.list_possible_moves(), self.opponent, 3) is not False:
            if len(evaluating_moves) == 0:
                break
            new_board.set_cell_to_empty(max_value[0])
            max_value = evaluating_moves.pop(
                evaluating_moves.index(max(evaluating_moves, key=lambda tup: tup[1])))
            new_board.set_cell(max_value[0], self.opponent)
        return max_value[0]

    def return_naive_utility(self, board, possible_moves, player):
        """ returns a list of moves and utility
        """
        output_list = []
        for move in possible_moves:
            move_utility = 0
            cell = board.get_cell(move)
            board.set_cell(move, player)
            player_data_dict = board.get_move_values(cell)
            for key, val in player_data_dict.items():
                openings_multiplier = 1.5
                if val['both_sides_open']:
                    openings_multiplier = 2
                chain_multiplier = 3
                if val['total_holes'] > 0:
                    chain_multiplier = 2.5
                if val['total_chain'] == 2 and val['both_sides_open']:
                    move_utility += 100
                move_utility += val['total_openings'] * openings_multiplier + \
                                val['total_chain'] * chain_multiplier + cell.center_weight
            output_list.append((move, move_utility))
            board.set_cell_to_empty(move)
        return output_list